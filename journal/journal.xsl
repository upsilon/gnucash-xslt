<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"

  xmlns:gnc="http://www.gnucash.org/XML/gnc"
  xmlns:act="http://www.gnucash.org/XML/act"
  xmlns:book="http://www.gnucash.org/XML/book"
  xmlns:cd="http://www.gnucash.org/XML/cd"
  xmlns:cmdty="http://www.gnucash.org/XML/cmdty"
  xmlns:price="http://www.gnucash.org/XML/price"
  xmlns:slot="http://www.gnucash.org/XML/slot"
  xmlns:split="http://www.gnucash.org/XML/split"
  xmlns:sx="http://www.gnucash.org/XML/sx"
  xmlns:trn="http://www.gnucash.org/XML/trn"
  xmlns:ts="http://www.gnucash.org/XML/ts"
  xmlns:fs="http://www.gnucash.org/XML/fs"
  xmlns:bgt="http://www.gnucash.org/XML/bgt"
  xmlns:recurrence="http://www.gnucash.org/XML/recurrence"
  xmlns:lot="http://www.gnucash.org/XML/lot"
  xmlns:addr="http://www.gnucash.org/XML/addr"
  xmlns:owner="http://www.gnucash.org/XML/owner"
  xmlns:billterm="http://www.gnucash.org/XML/billterm"
  xmlns:bt-days="http://www.gnucash.org/XML/bt-days"
  xmlns:bt-prox="http://www.gnucash.org/XML/bt-prox"
  xmlns:cust="http://www.gnucash.org/XML/cust"
  xmlns:employee="http://www.gnucash.org/XML/employee"
  xmlns:entry="http://www.gnucash.org/XML/entry"
  xmlns:invoice="http://www.gnucash.org/XML/invoice"
  xmlns:job="http://www.gnucash.org/XML/job"
  xmlns:order="http://www.gnucash.org/XML/order"
  xmlns:taxtable="http://www.gnucash.org/XML/taxtable"
  xmlns:tte="http://www.gnucash.org/XML/tte"
  xmlns:vendor="http://www.gnucash.org/XML/vendor">

<xsl:template match="/gnc-v2">
  <html>
  <head>
  <link rel="stylesheet" type="text/css" href="journal.css"/>
  <link rel="stylesheet" type="text/css" href="journal.print.css" media="print"/>
  </head>
  <body>
  <table class="book">
    <caption><span class="title">仕訳帳</span></caption>
    <colgroup>
      <col class="date"/>
    </colgroup>
    <colgroup class="account">
      <col class="debit"/>
      <col class="credit"/>
    </colgroup>
    <colgroup class="value">
      <col class="debit"/>
      <col class="credit"/>
    </colgroup>
    <thead>
      <tr>
      <th>日付</th>
      <th colspan="2">摘要</th>
      <th>借方</th>
      <th>貸方</th>
      </tr>
    </thead>
    <tbody>
      <xsl:apply-templates select="gnc:book/gnc:transaction">
        <xsl:sort select="trn:date-posted/ts:date"/>
        <xsl:sort select="trn:num"/>
      </xsl:apply-templates>
    </tbody>
  </table>
  </body>
  </html>
</xsl:template>

<xsl:key name="act-key" match="gnc:account" use="act:id"/>

<xsl:template match="gnc:transaction">
  <xsl:for-each select="trn:splits/trn:split">
  <tr class="transaction">
    <xsl:if test="position() = 1">
    <td><xsl:value-of select="substring-before(../../trn:date-posted/ts:date, ' ')"/></td>
    </xsl:if>
    <xsl:if test="position() &gt; 1">
    <td/>
    </xsl:if>

    <xsl:variable name="split-account" select="key('act-key', split:account)[1]/act:name"/>
    <xsl:variable name="split-value" select="substring-before(split:value, '/') div substring-after(split:value, '/')"/>

    <xsl:choose>
    <xsl:when test="$split-value &gt; 0">
    <td><span class="account"><xsl:value-of select="$split-account"/></span></td>
    <td/>
    <td class="value"><xsl:value-of select="format-number($split-value, '#,###')"/></td>
    <td/>
    </xsl:when>
    <xsl:when test="$split-value &lt; 0">
    <td/>
    <td><span class="account"><xsl:value-of select="$split-account"/></span></td>
    <td/>
    <td class="value"><xsl:value-of select="format-number(-$split-value, '#,###')"/></td>
    </xsl:when>
    <xsl:otherwise>
    <td><span class="account"><xsl:value-of select="$split-account"/></span></td>
    <td/>
    <td/>
    <td/>
    </xsl:otherwise>
    </xsl:choose>
  </tr>
  </xsl:for-each>
  <tr class="trn trn-last">
    <td/>
    <td class="description" colspan="2"><xsl:value-of select="trn:description"/></td>
    <td/>
    <td/>
  </tr>
</xsl:template>

</xsl:stylesheet>
